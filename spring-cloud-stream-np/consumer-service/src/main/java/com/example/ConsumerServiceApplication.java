package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.SubscribableChannel;


@SpringBootApplication
@EnableBinding(ConsumerChannels.class)
public class ConsumerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerServiceApplication.class, args);
	}
    
    @StreamListener (ConsumerChannels.PRODUCER)
    public void receive (String msg) {
        System.out.println("*** Received message: " + msg);
    }
    
}


interface ConsumerChannels {
    String PRODUCER = "producer";
    
    @Input (PRODUCER)
    SubscribableChannel producer();
}