package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

@RestController
@EnableBinding(ProducerChannels.class)
@SpringBootApplication
public class ProducerApplication {

    private final MessageChannel consumer;
    
//    @Autowired
    public ProducerApplication(ProducerChannels channels) {
        this.consumer = channels.consumer();
    }
    
		
    @PostMapping("/greet-post/{name}")
    public void publish (@PathVariable String name) {
        String greeting = "Hello, " + name + "!" ;
        Message <String> msg = MessageBuilder.withPayload(greeting).build();
        this.consumer.send(msg);
    }
    
		@RequestMapping("/greet-get/{name}")
    public String read (@PathVariable String name) {
        String greeting = "Hello, " + name + "!" ;
        Message <String> msg = MessageBuilder.withPayload(greeting).build();
        this.consumer.send(msg);
				return greeting;
    }
		
	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}
}

interface ProducerChannels {
    
    @Output
    MessageChannel consumer();
}