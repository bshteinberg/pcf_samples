package com.example;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SleuthApp2Application {
		
    private static final Log log = LogFactory.getLog(SleuthApp2Application.class);
    
    @RequestMapping("/getinfo/{id}")
    public String read (@PathVariable String id) {
        String info = "Info for id: " + id + "!" ;
        log.info(info);
        return info;
    }
    
    @RequestMapping("/get")
    public String get2 () {
        String info = "App2 called...";
        log.info(info);
        return info;
    }
		
	public static void main(String[] args) {
		SpringApplication.run(SleuthApp2Application.class, args);
	}
}
