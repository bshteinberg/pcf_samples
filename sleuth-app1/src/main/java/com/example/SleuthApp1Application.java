package com.example;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@EnableFeignClients
@SpringBootApplication
public class SleuthApp1Application {
		
    private static final Log log = LogFactory.getLog(SleuthApp1Application.class);
    
    @Autowired
    SleuthApp2Client sleuthApp2;
    
    @RequestMapping("/getstuff/{id}")
    public String read (@PathVariable String id) {
        log.info("/getstuff/{id} invoked !");
        //        String info = "Stuff --> " + sleuthApp2.getinfo(id) + "!" ;
        String info = "Stuff --> " + sleuthApp2.get2() + "!" ;
        log.info(info);
        return info;
    }
		
	public static void main(String[] args) {
		SpringApplication.run(SleuthApp1Application.class, args);
	}
    
    @FeignClient (name="sleuth-app2", url="https://sleuth-app2.app.52.232.132.166.cf.pcfazure.com")
    interface SleuthApp2Client {
        
//        @RequestMapping("/getinfo/{id}")
//        String getinfo (@PathVariable("id") String id);
        
        @RequestMapping(value = "/get", method = GET)
        String get2 ();
        
        
    
    }
}